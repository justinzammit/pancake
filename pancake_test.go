package pancake

import (
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestFlatMap(t *testing.T) {
	tests := []struct {
		name        string
		vars        map[string]interface{}
		full        bool
		expectedMap map[string]interface{}
	}{
		{
			name: "linear map full",
			vars: map[string]interface{}{
				"department": "tech",
				"skillLevel": 5,
				"languages":  []interface{}{"nl", "en"},
				"available":  true,
			},
			full: true,
			expectedMap: map[string]interface{}{
				"department":   "tech",
				"skillLevel":   5,
				"languages":    []interface{}{"nl", "en"},
				"languages[0]": "nl",
				"languages[1]": "en",
				"available":    true,
			},
		},
		{
			name: "nested map full",
			vars: map[string]interface{}{
				"agent": map[string]interface{}{
					"department": "tech",
					"skillLevel": 5,
					"languages":  []interface{}{"nl", "en"},
					"available":  true,
				},
			},
			full: true,
			expectedMap: map[string]interface{}{
				"agent": map[string]interface{}{
					"department": "tech",
					"skillLevel": 5,
					"languages":  []interface{}{"nl", "en"},
					"available":  true,
				},
				"agent.department":   "tech",
				"agent.skillLevel":   5,
				"agent.languages":    []interface{}{"nl", "en"},
				"agent.languages[0]": "nl",
				"agent.languages[1]": "en",
				"agent.available":    true,
			},
		},
		{
			name: "deep nested map full",
			vars: map[string]interface{}{
				"agent": map[string]interface{}{
					"empty":      nil,
					"department": "tech",
					"skillLevel": 5,
					"foo": map[string]interface{}{
						"bar": "baz",
						"yolo": map[string]interface{}{
							"swag": "value",
						},
					},
					"languages": []interface{}{
						"nl",
						"en",
						map[string]interface{}{
							"more": []interface{}{"mt", "jp"},
						},
						[]interface{}{"first", "second", "third"},
					},
					"available": true,
				},
			},
			full: true,
			expectedMap: map[string]interface{}{
				"agent": map[string]interface{}{
					"empty":      nil,
					"department": "tech",
					"skillLevel": 5,
					"foo": map[string]interface{}{
						"bar": "baz",
						"yolo": map[string]interface{}{
							"swag": "value",
						},
					},
					"languages": []interface{}{
						"nl",
						"en",
						map[string]interface{}{
							"more": []interface{}{"mt", "jp"},
						},
						[]interface{}{"first", "second", "third"},
					},
					"available": true,
				},
				"agent.empty":      nil,
				"agent.department": "tech",
				"agent.skillLevel": 5,
				"agent.foo": map[string]interface{}{
					"bar": "baz",
					"yolo": map[string]interface{}{
						"swag": "value",
					},
				},
				"agent.foo.bar": "baz",
				"agent.foo.yolo": map[string]interface{}{
					"swag": "value",
				},
				"agent.foo.yolo.swag": "value",
				"agent.languages": []interface{}{
					"nl",
					"en",
					map[string]interface{}{
						"more": []interface{}{"mt", "jp"},
					},
					[]interface{}{"first", "second", "third"},
				},
				"agent.languages[0]": "nl",
				"agent.languages[1]": "en",
				"agent.languages[2]": map[string]interface{}{
					"more": []interface{}{"mt", "jp"},
				},
				"agent.languages[2].more":    []interface{}{"mt", "jp"},
				"agent.languages[2].more[0]": "mt",
				"agent.languages[2].more[1]": "jp",
				"agent.languages[3]":         []interface{}{"first", "second", "third"},
				"agent.languages[3][0]":      "first",
				"agent.languages[3][1]":      "second",
				"agent.languages[3][2]":      "third",
				"agent.available":            true,
			},
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			actual := FlatMap(tt.vars, tt.full)
			require.Equal(t, tt.expectedMap, actual)
		})
	}
}

func TestFlatJSONDetailed(t *testing.T) {
	tests := []struct {
		name         string
		rawJSON      json.RawMessage
		expectedJSON string
		expectedErr  error
	}{
		{
			name:         "map linear full",
			rawJSON:      []byte(`{"foo":"bar"}`),
			expectedJSON: `{"foo":"bar"}`,
			expectedErr:  nil,
		},
		{
			name:         "map nested full",
			rawJSON:      []byte(`{"language":{"mt_MT":{"level":10},"nl_NL":{"level":2}},"skill":{"patience":{"level":7}},"custom":{"arr":["foo",45,true,"bar"]}}`),
			expectedJSON: `{"custom":{"arr":["foo",45,true,"bar"]},"custom.arr":["foo",45,true,"bar"],"custom.arr[0]":"foo","custom.arr[1]":45,"custom.arr[2]":true,"custom.arr[3]":"bar","language":{"mt_MT":{"level":10},"nl_NL":{"level":2}},"language.mt_MT":{"level":10},"language.mt_MT.level":10,"language.nl_NL":{"level":2},"language.nl_NL.level":2,"skill":{"patience":{"level":7}},"skill.patience":{"level":7},"skill.patience.level":7}`,
			expectedErr:  nil,
		},
		{
			name:         "map nested with map inside array full",
			rawJSON:      []byte(`{"language":{"mt_MT":{"level":10},"nl_NL":{"level":2}},"skill":{"patience":{"level":7}},"custom":{"arr":["foo",{"favouriteNumber":7,"nestedArray":[4,5,"zeven","ein"]},45,true,"bar"]}}`),
			expectedJSON: `{"custom":{"arr":["foo",{"favouriteNumber":7,"nestedArray":[4,5,"zeven","ein"]},45,true,"bar"]},"custom.arr":["foo",{"favouriteNumber":7,"nestedArray":[4,5,"zeven","ein"]},45,true,"bar"],"custom.arr[0]":"foo","custom.arr[1]":{"favouriteNumber":7,"nestedArray":[4,5,"zeven","ein"]},"custom.arr[1].favouriteNumber":7,"custom.arr[1].nestedArray":[4,5,"zeven","ein"],"custom.arr[1].nestedArray[0]":4,"custom.arr[1].nestedArray[1]":5,"custom.arr[1].nestedArray[2]":"zeven","custom.arr[1].nestedArray[3]":"ein","custom.arr[2]":45,"custom.arr[3]":true,"custom.arr[4]":"bar","language":{"mt_MT":{"level":10},"nl_NL":{"level":2}},"language.mt_MT":{"level":10},"language.mt_MT.level":10,"language.nl_NL":{"level":2},"language.nl_NL.level":2,"skill":{"patience":{"level":7}},"skill.patience":{"level":7},"skill.patience.level":7}`,
			expectedErr:  nil,
		},
		{
			name:         "map nested with array inside array full",
			rawJSON:      []byte(`{"language":{"mt_MT":{"level":10},"nl_NL":{"level":2}},"skill":{"patience":{"level":7}},"custom":{"arr":["foo",{"favouriteNumber":7,"nestedArray":[4,5,"zeven","ein"]},45,true,"bar",["tnejn","tlieta","sitta"]]}}`),
			expectedJSON: `{"custom":{"arr":["foo",{"favouriteNumber":7,"nestedArray":[4,5,"zeven","ein"]},45,true,"bar",["tnejn","tlieta","sitta"]]},"custom.arr":["foo",{"favouriteNumber":7,"nestedArray":[4,5,"zeven","ein"]},45,true,"bar",["tnejn","tlieta","sitta"]],"custom.arr[0]":"foo","custom.arr[1]":{"favouriteNumber":7,"nestedArray":[4,5,"zeven","ein"]},"custom.arr[1].favouriteNumber":7,"custom.arr[1].nestedArray":[4,5,"zeven","ein"],"custom.arr[1].nestedArray[0]":4,"custom.arr[1].nestedArray[1]":5,"custom.arr[1].nestedArray[2]":"zeven","custom.arr[1].nestedArray[3]":"ein","custom.arr[2]":45,"custom.arr[3]":true,"custom.arr[4]":"bar","custom.arr[5]":["tnejn","tlieta","sitta"],"custom.arr[5][0]":"tnejn","custom.arr[5][1]":"tlieta","custom.arr[5][2]":"sitta","language":{"mt_MT":{"level":10},"nl_NL":{"level":2}},"language.mt_MT":{"level":10},"language.mt_MT.level":10,"language.nl_NL":{"level":2},"language.nl_NL.level":2,"skill":{"patience":{"level":7}},"skill.patience":{"level":7},"skill.patience.level":7}`,
			expectedErr:  nil,
		},
		{
			name:         "slice nested with map full",
			rawJSON:      []byte(`["foo",{"item1":"one","arr":[55,{"arrObj":10}]},["first",{"secondObj":"secondObj string"}],"baz"]`),
			expectedJSON: `{"[0]":"foo","[1]":{"arr":[55,{"arrObj":10}],"item1":"one"},"[1].arr":[55,{"arrObj":10}],"[1].arr[0]":55,"[1].arr[1]":{"arrObj":10},"[1].arr[1].arrObj":10,"[1].item1":"one","[2]":["first",{"secondObj":"secondObj string"}],"[2][0]":"first","[2][1]":{"secondObj":"secondObj string"},"[2][1].secondObj":"secondObj string","[3]":"baz"}`,
			expectedErr:  nil,
		},
		{
			name:         "invalid json format",
			rawJSON:      []byte(`"foo"`),
			expectedJSON: ``,
			expectedErr:  errors.New("flatmapper: invalid json format (must be an object or array)"),
		},
		{
			name:         "invalid json",
			rawJSON:      []byte(`{`),
			expectedJSON: ``,
			expectedErr:  errors.New("flatmapper: could not unmarshal raw json: unexpected end of JSON input"),
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			actual, err := FlatJSONDetailed(tt.rawJSON)

			require.Equal(t, tt.expectedJSON, string(actual))
			require.Equal(t, tt.expectedErr, err)
		})
	}
}


func TestFlatJSONSimple(t *testing.T) {
	tests := []struct {
		name         string
		rawJSON      json.RawMessage
		expectedJSON string
		expectedErr  error
	}{
		{
			name:         "map nested with array inside array",
			rawJSON:      []byte(`{"name":"Justin","language":{"mt_MT":{"level":10},"nl_NL":{"level":2}},"skill":{"patience":{"level":7}},"custom":{"arr":["foo",{"favouriteNumber":7,"nestedArray":[4,5,"zeven","ein"]},45,true,"bar",["tnejn","tlieta","sitta"]]}}`),
			expectedJSON: `{"custom.arr":["foo",45,true,"bar"],"custom.arr[0]":"foo","custom.arr[1].favouriteNumber":7,"custom.arr[1].nestedArray":[4,5,"zeven","ein"],"custom.arr[1].nestedArray[0]":4,"custom.arr[1].nestedArray[1]":5,"custom.arr[1].nestedArray[2]":"zeven","custom.arr[1].nestedArray[3]":"ein","custom.arr[2]":45,"custom.arr[3]":true,"custom.arr[4]":"bar","custom.arr[5]":["tnejn","tlieta","sitta"],"custom.arr[5][0]":"tnejn","custom.arr[5][1]":"tlieta","custom.arr[5][2]":"sitta","language.mt_MT.level":10,"language.nl_NL.level":2,"name":"Justin","skill.patience.level":7}`,
			expectedErr:  nil,
		},
		{
			name:         "slice linear",
			rawJSON:      []byte(`["foo","bar","baz"]`),
			expectedJSON: `{"[0]":"foo","[1]":"bar","[2]":"baz"}`,
			expectedErr:  nil,
		},
		{
			name:         "slice nested with map",
			rawJSON:      []byte(`["foo",{"item1":"one","arr":[55,{"arrObj":10}]},["first",{"secondObj":"secondObj string"}],"baz"]`),
			expectedJSON: `{"[0]":"foo","[1].arr":[55],"[1].arr[0]":55,"[1].arr[1].arrObj":10,"[1].item1":"one","[2]":["first"],"[2][0]":"first","[2][1].secondObj":"secondObj string","[3]":"baz"}`,
			expectedErr:  nil,
		},
		{
			name:         "invalid json format",
			rawJSON:      []byte(`"foo"`),
			expectedJSON: ``,
			expectedErr:  errors.New("flatmapper: invalid json format (must be an object or array)"),
		},
		{
			name:         "invalid json",
			rawJSON:      []byte(`{`),
			expectedJSON: ``,
			expectedErr:  errors.New("flatmapper: could not unmarshal raw json: unexpected end of JSON input"),
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			actual, err := FlatJSONSimple(tt.rawJSON)

			require.Equal(t, tt.expectedJSON, string(actual))
			require.Equal(t, tt.expectedErr, err)
		})
	}
}
