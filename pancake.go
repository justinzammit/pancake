package pancake

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
)

func flatSlice(slice []interface{}, flat map[string]interface{}, currentKey string, full bool) {
	// Used to store this slice in the flat map.
	sliceCopy := make([]interface{}, 0)

	for index, val := range slice {
		// Append key accordingly.
		flatKey := currentKey + "[" + strconv.Itoa(index) + "]"
		switch v := val.(type) {
		case []interface{}:
			// Slice.
			if full {
				// Add slice if applicable.
				sliceCopy = append(sliceCopy, v)
			}
			flatSlice(v, flat, flatKey, full)
		case map[string]interface{}:
			// Map.
			if full {
				// Add map if applicable.
				flat[flatKey] = v
				sliceCopy = append(sliceCopy, v)
			}
			flatMap(v, flat, flatKey, full)
		default:
			// String, number or boolean.
			sliceCopy = append(sliceCopy, v)
			flat[flatKey] = v
		}
	}

	// Add the slice in the linear map if applicable.
	if currentKey != "" {
		flat[currentKey] = sliceCopy
	}
}

func flatMap(m, flat map[string]interface{}, currentKey string, full bool) {
	for key, val := range m {
		// Append key accordingly.
		var flatKey string
		if currentKey == "" {
			flatKey = key
		} else {
			flatKey = currentKey + "." + key
		}

		switch v := val.(type) {
		case []interface{}:
			// Slice.
			flatSlice(v, flat, flatKey, full)
		case map[string]interface{}:
			// Map.
			if full {
				// Add map if applicable.
				flat[flatKey] = v
			}
			flatMap(v, flat, flatKey, full)
		default:
			// String, number or boolean.
			flat[flatKey] = v
		}
	}
}

// FlatSlice converts a an array into an object with keys being the indexes. The keys of the linear map will be
// represented with square brackets describing the index.
// Example []string{"foo"} -> map[string]interface{}{"[0]": "foo"}
func FlatSlice(slice []interface{}, full bool) map[string]interface{} {
	flat := make(map[string]interface{})
	flatSlice(slice, flat, "", full)
	return flat
}

// FlatMap converts a nested map to a linear one. The keys of the linear map will be assigned with a dot notation,
// describing the nested structure.
// Example
// map[string]interface{}{
//     "language": map[string]interface{}{
//		    "nl": "good"
//	   }
// }
// will become map[string]interface{}{"language.nl": "good"}
func FlatMap(m map[string]interface{}, full bool) map[string]interface{} {
	flat := make(map[string]interface{})
	flatMap(m, flat, "", full)
	return flat
}

// FlatJSON converts a raw nested JSON into a linear JSON.
func FlatJSON(rawJSON json.RawMessage, full bool) ([]byte, error) {
	var i interface{}
	if err := json.Unmarshal(rawJSON, &i); err != nil {
		return nil, fmt.Errorf("pancake: could not unmarshal raw json: %v", err)
	}

	var flat interface{}
	switch val := i.(type) {
	case map[string]interface{}:
		if full {
			flat = FlatMapDetailed(val)
		} else {
			flat = FlatMap(val, full)
			flat = FlatMapSimple(val)
		}
	case []interface{}:
		if full {
			flat = FlatSliceDetailed(val)
		} else {
			flat = FlatSliceSimple(val)
		}
	default:
		return nil, errors.New("pancake: invalid json format (must be an object or array)")
	}

	flatMapBytes, err := json.Marshal(flat)
	if err != nil {
		return nil, fmt.Errorf("pancake: could not marshal flat map: %v", err)
	}

	return flatMapBytes, nil
}

// FlatMapDetailed is an extension to FlatMap.
func FlatMapDetailed(m map[string]interface{}) map[string]interface{} {
	return FlatMap(m, true)
}

// FlatSliceDetailed is an extension to FlatSlice.
func FlatSliceDetailed(slice []interface{}) map[string]interface{} {
	return FlatSlice(slice, true)
}

// FlatJSONDetailed is an extension to FlatJSON.
func FlatJSONDetailed(rawJSON json.RawMessage) ([]byte, error) {
	return FlatJSON(rawJSON, true)
}

// FlatMapSimple is an extension to FlatMap.
func FlatMapSimple(m map[string]interface{}) map[string]interface{} {
	return FlatMap(m, false)
}

// FlatSliceSimple is an extension to FlatSlice.
func FlatSliceSimple(slice []interface{}) map[string]interface{} {
	return FlatSlice(slice, false)
}

// FlatJSONSimple is an extension to FlatJSON.
func FlatJSONSimple(rawJSON json.RawMessage) ([]byte, error) {
	return FlatJSON(rawJSON, false)
}
